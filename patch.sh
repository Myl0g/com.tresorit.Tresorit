#! /bin/sh

sed -i 's|$HOME/.local/share/tresorit|/app/tresorit|g' tresorit_installer.run
sed -i 's|read CHANGE_INSTALL_DIR|CHANGE_INSTALL_DIR=N|g' tresorit_installer.run
sed -i 's|mv "$INSTALL_DIR/tresorit.desktop" "$HOME/.local/share/applications/"|mv "$INSTALL_DIR/tresorit.desktop" "/app/share/applications/com.tresorit.Tresorit.desktop"|g' tresorit_installer.run
sed -i 's|$HOME/.local/share/applications/|/app/share/applications/|g' tresorit_installer.run
sed -i 's|mkdir -p "$HOME/.local/share/applications"|mkdir -p "/app/share/applications"|g' tresorit_installer.run
sed -i 's|read START_TRESORIT|START_TRESORIT=n|g' tresorit_installer.run
